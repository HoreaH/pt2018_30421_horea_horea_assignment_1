package PT2018.demo.DemoProject;

import org.junit.Test;

import PT2018.demo.DemoProject.model.Polynomial;

public class TestCase extends junit.framework.TestCase {

	@Test
	public void testAdd()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		
		Polynomial b = new Polynomial();
		b.make("0 -7 3 3 0 1");
		
		Polynomial r = a.sum(b);
		assert r.toString().equals("1.0 -5.0x^1 +6.0x^2 +7.0x^3 +5.0x^4 +x^5");
		
		a = new Polynomial();
		a.make("0");
		
		Polynomial r2 = r.sum(a);
		assert r.toString().equals(r2.toString());
		
		a = new Polynomial();
		a.make("");
		
		r2 = r.sum(a);
		assert r.toString().equals(r2.toString());
		
		a = new Polynomial();
		a.make("1");
		
		b = new Polynomial();
		b.make("-1");
		
		r = a.sum(b);
		
		assert r.toString().equals("0");
	}
	
	@Test
	public void testSub()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		
		Polynomial b = new Polynomial();
		b.make("0 -7 3 3 0 1");
		
		Polynomial r = a.sub(b);
		assert r.toString().equals("1.0 +9.0x^1 +x^3+5.0x^4 -1.0x^5 ");
		
		a = new Polynomial();
		a.make("0");
		
		Polynomial r2 = r.sub(a);
		assert r.toString().equals(r2.toString());
		
		a = new Polynomial();
		a.make("");
		
		r2 = r.sub(a);
		assert r.toString().equals(r2.toString());
		
		a = new Polynomial();
		a.make("1");
		
		b = new Polynomial();
		b.make("1");
		
		r = a.sub(b);
		
		assert r.toString().equals("0");
	}
	
	@Test
	public void testMul()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		
		Polynomial b = new Polynomial();
		b.make("0 -7 3 3 0 1");
		
		Polynomial r = a.mul(b);
		
		assert r.toString().equals("-7.0x^1 -11.0x^2 -12.0x^3 -13.0x^4 -13.0x^5 +29.0x^6 +18.0x^7 +4.0x^8 +5.0x^9 ");
		
		a = new Polynomial();
		a.make("0");
		
		Polynomial r2 = r.mul(a);
		assert r2.toString().equals("0");
		
		a = new Polynomial();
		a.make("1");
		
		r2 = r.mul(a);
		assert r2.toString().equals(r.toString());
		
		a = new Polynomial();
		a.make("-1");
		
		r2 = r.mul(a);
		
		Polynomial r3 = r.sub(r);
		Polynomial r4 = r3.sub(r);
		
		assert r2.toString().equals(r4.toString());
	}
	
	@Test
	public void testDiv()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		
		Polynomial b = new Polynomial();
		b.make("0 -7 3 3 0 1");
		
		String r = b.div(a);
		assert r.equals("Quotient: -0.16 +0.2x^1  Remainder: 0.16 -6.88x^1 +3.08x^2 +3.04x^3 ");
		
		a = new Polynomial();
		a.make("0");
		
		String r2 = b.div(a);
		assert r2.toString().equals("Division by zero!");
		
		
		a = new Polynomial();
		a.make("391 34 46 142 35 2");
		
		b = new Polynomial();
		b.make("23 2");
		
		r = a.div(b);
		assert r.equals("Quotient: 17.0 +2.0x^2 +6.0x^3 +x^4 Remainder: 0");
		
		a = new Polynomial();
		a.make("391 34 46 142 35 2");
		
		b = new Polynomial();
		b.make("391 34 46 142 35 2");
		r = a.div(b);
		//System.out.println(r);
		assert r.equals("Quotient: 1.0  Remainder: 0");
	}
	
	@Test
	public void testDiff()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		Polynomial r = a.diff();
		//System.out.println(r.toString());
		assert r.toString().equals("2.0 +6.0x^1 +12.0x^2 +20.0x^3 ");
		
		a = new Polynomial();
		a.make("666");
		r = a.diff();
		assert r.toString().equals("0");
	}
	
	@Test
	public void testInteg()
	{
		Polynomial a = new Polynomial();
		a.make("1 2 3 4 5");
		Polynomial r = a.integ();
		assert r.toString().equals("x^1+x^2+x^3+x^4+x^5");
	}
	
}
