package PT2018.demo.DemoProject.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import PT2018.demo.DemoProject.model.Polynomial;
import PT2018.demo.DemoProject.view.*;


public class Controller implements ActionListener{

    private View view;
    Polynomial poly1 = new Polynomial();
    Polynomial poly2 = new Polynomial();
    public Controller(View view/*,Polynomial[] model*/) {
        this.view = view;
        //poly1 = model[0];
       // poly2 = model[1];
        view.calculate.addActionListener(this);
    }

    //@Override
    public void actionPerformed(ActionEvent e) {
        recalculate();
    }

    private void recalculate(){
        String input1 = this.view.textFieldP1.getText();
        String input2 = this.view.textFieldP2.getText(); 
        
        if (input1.matches("[0-9,+, ,-]+") && input2.matches("[0-9,+, ,-]+"))
        {
        poly1.make(input1);
        poly2.make(input2);
        

        this.view.addField.setText(poly1.sum(poly2).toString());
        this.view.subField.setText(poly1.sub(poly2).toString());
        this.view.multiField.setText(poly1.mul(poly2).toString());
        if (poly1.div(poly2)!= null)
          {
          this.view.diviField.setText(poly1.div(poly2).toString());
          }
        else
        {
          this.view.diviField.setText("Division by 0!");
        }
        this.view.integField.setText(poly1.integ().toString());
        poly1 = new Polynomial();
        poly1.make(input1);
        this.view.diffField.setText(poly1.diff().toString());
        
        //this.view.addField.setText(input1);
        //this.view.subField.setText(input2);
        }
        else
        {
          this.view.addField.setText("Invalid operands!");
          this.view.subField.setText("Invalid operands!");
          this.view.diviField.setText("Division by 0!");//I guess it's more correct
          this.view.multiField.setText("0");
          if (input1.matches("[0-9,+, ,-]+"))
            {
                poly1.make(input1);
                this.view.integField.setText(poly1.integ().toString());
                poly1 = new Polynomial();
                poly1.make(input1);
                this.view.diffField.setText(poly1.diff().toString());
            }
          else
            {
            this.view.integField.setText("Invalid input!");
            this.view.diffField.setText("Invalid input!");
            }
        }
          
    }
}
