package PT2018.demo.DemoProject.view;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame {
    private static final int WHEIGHT = 400;
    private static final int WWIDTH = 800;
    private JPanel mainPanel;

    private JPanel panelInput;
    private JPanel panelOutput;

    private JLabel polynomial1;
    private JLabel polynomial2;

    private JLabel addLabel;
    private JLabel subLabel;
    private JLabel integLabel;
    private JLabel diffLabel;

    private JLabel multiLabel;
    private JLabel diviLabel;

    public JButton calculate;//button to calculate with actionList

    public JTextField textFieldP1;//input field1
    public JTextField textFieldP2;//input field2

    public JTextField addField;
    public JTextField subField;
    public JTextField integField;
    public JTextField diffField;
    public JTextField multiField;
    public JTextField diviField;


    public View()  {
        draw();
    }

    public final void draw(){
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.X_AXIS));

        panelInput = new JPanel();
        panelInput.setLayout(new BoxLayout(panelInput,BoxLayout.Y_AXIS));
        panelInput.setMinimumSize(new Dimension(5*WWIDTH/10,WHEIGHT));

        panelOutput = new JPanel();
        panelOutput.setLayout(new BoxLayout(panelOutput,BoxLayout.Y_AXIS));
        panelOutput.setMinimumSize(new Dimension(5*WWIDTH/10,WHEIGHT));

        /*Input*/
        polynomial1 = new JLabel();
        polynomial1.setVisible(true);
        polynomial1.setText("First Polynomial:");
        textFieldP1 = new JTextField(200);
        textFieldP1.setMaximumSize(new Dimension(WWIDTH,1*WHEIGHT/10));
        textFieldP1.setVisible(true);

        polynomial2 = new JLabel();
        polynomial2.setVisible(true);
        polynomial2.setText("Second Polynomial:");
        textFieldP2 = new JTextField(200);
        textFieldP2.setVisible(true);
        textFieldP2.setMaximumSize(new Dimension(WWIDTH,1*WHEIGHT/10));

        calculate = new JButton();
        calculate.setVisible(true);
        calculate.setText("Calculate");
        calculate.setMaximumSize(new Dimension(5*WWIDTH,1*WHEIGHT/10));
        calculate.setMinimumSize(new Dimension(5*WWIDTH/10,1*WHEIGHT/10));

        panelInput.add(polynomial1);
        panelInput.add(textFieldP1);
        panelInput.add(polynomial2);
        panelInput.add(textFieldP2);
        panelInput.add(calculate);


        /*Output*/
        addField =new JTextField(200);
        subField=new JTextField(200);
        multiField=new JTextField(200);
        diviField=new JTextField(200);
        integField=new JTextField(200);
        diffField=new JTextField(200);

        addField.setVisible(true);
        subField.setVisible(true);
        multiField.setVisible(true);
        diviField.setVisible(true);
        integField.setVisible(true);
        diffField.setVisible(true);

        addField.setEnabled(false);
        subField.setEnabled(false);
        multiField.setEnabled(false);
        diviField.setEnabled(false);
        integField.setEnabled(false);
        diffField.setEnabled(false);

        addLabel=new JLabel();
        subLabel=new JLabel();
        multiLabel=new JLabel();
        diviLabel=new JLabel();
        integLabel=new JLabel();
        diffLabel=new JLabel();

        addLabel.setVisible(true);
        subLabel.setVisible(true);
        multiLabel.setVisible(true);
        diviLabel.setVisible(true);
        integLabel.setVisible(true);
        diffLabel.setVisible(true);

        addLabel.setText("Addition Result:");
        subLabel.setText("Subtraction Result:");
        multiLabel.setText("Multiplication Result:");
        diviLabel.setText("Division Result:");
        integLabel.setText("Integration Result:");
        diffLabel.setText("Differentiation Result:");

        panelOutput.add(addLabel);
        panelOutput.add(addField);
        panelOutput.add(subLabel);
        panelOutput.add(subField);
        panelOutput.add(multiLabel);
        panelOutput.add(multiField);
        panelOutput.add(diviLabel);
        panelOutput.add(diviField);
        panelOutput.add(integLabel);
        panelOutput.add(integField);
        panelOutput.add(diffLabel);
        panelOutput.add(diffField);
        
        //to do separate vertically
        panelOutput.add(new JSeparator(SwingConstants.HORIZONTAL));//useless

        mainPanel.add(panelInput);
        mainPanel.add(panelOutput);

        setContentPane(mainPanel);
        setTitle("Polynomial Calculator");
        setSize(WWIDTH, WHEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
