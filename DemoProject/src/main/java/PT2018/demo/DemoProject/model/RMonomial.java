package PT2018.demo.DemoProject.model;

public class RMonomial extends Monomial implements Cloneable{

  
  public RMonomial(int deg,double coef) {
    this.deg = deg;
    this.coef = coef;
  }
  
  public Double getCoef() {
    return  coef.doubleValue();
  }

  public void setCoef(Double coef) {
    this.coef = coef;
  }

  @Override
  protected Object clone(){
    return new RMonomial(this.deg,this.coef.doubleValue());
  }

  @Override
  public String toString() {
    if (this.deg == 0)
      {
      return this.coef.floatValue() + " ";
      }
    else if(this.coef.floatValue() != 1f)
      {
      return this.coef.floatValue() + "x^" + this.deg + " ";
      }
    return "x^" + this.deg;
  }

  public int compareTo(Object o) {
    return Integer.compare(this.deg, ((Monomial)o).deg);
  }
  
}
