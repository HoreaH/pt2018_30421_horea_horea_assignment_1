package PT2018.demo.DemoProject.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class Polynomial {

  TreeSet<Monomial> poly;

  public Polynomial(TreeSet<Monomial> poly) {
    this.poly = poly;
  }

  public Polynomial() {

  }
  /*public TreeSet<Monomial> clone() {

    TreeSet<Monomial> poly2 = new TreeSet<Monomial>();
    for (Monomial i : poly){
      poly2.add(new RMonomial(i.deg,i.coef.doubleValue()));
      }
    return poly2;
  }*/

  public TreeSet<Monomial> getPoly() {
    return poly;
  }

  public int size() {
    return  poly.size();
  }

  public void make(String S) {

    Scanner sc = new Scanner(S);
    TreeSet<Monomial> monoms = new TreeSet<Monomial>();
    int currentdeg = 0,currentcoef;

    while (sc.hasNextInt())
    {
      currentcoef = sc.nextInt();
      if (currentcoef != 0)
      {
        Monomial aux = new IMonomial(currentdeg,currentcoef);
        monoms.add(aux);
      }
      currentdeg++;
    }
    
    if (monoms.size() == 0)
    {
    	monoms.add(new IMonomial(0,0));
    }


    setPoly(monoms);

  }

  public void setPoly(TreeSet<Monomial> poly) {
    this.poly = poly;
  }

  public void TSAdd(TreeSet<Monomial> orig,int index,double newval)
  {
    if (orig.lower(new RMonomial(index+1,0f)) != null )
      if (orig.lower(new RMonomial(index+1,0f)).getDeg().intValue() == index)
      {
        double aux = orig.lower(new RMonomial(index+1,0f)).coef.doubleValue();
        orig.remove(new RMonomial(index,0f));
        if (newval + aux != 0)
          {
            orig.add(new RMonomial(index,newval + aux));
          }
        return;
      }

    if (newval!= 0)
      {
        orig.add(new RMonomial(index,newval));
      }

  }

  public Polynomial sum(Polynomial p) {

    TreeSet<Monomial> lh = new TreeSet<Monomial>(),rh;

    if (p.size() == 0)
      {
        lh.add(new RMonomial(0,0));
        return new Polynomial(lh);
      }

    lh = (TreeSet<Monomial>) poly.clone();
    rh = (TreeSet<Monomial>) p.poly.clone();
    /*if (p.poly.last().deg > poly.last().deg )
      {
        lh = (TreeSet<Monomial>) p.poly.clone();
        rh = (TreeSet<Monomial>) poly.clone();
      }
    else
      {
        rh = (TreeSet<Monomial>) p.poly.clone();
        lh = (TreeSet<Monomial>) poly.clone();
      }*/

    for (Monomial j : rh)
      {
        TSAdd(lh,j.deg,(j.coef).doubleValue());
      }

    return new Polynomial(lh);
  }

  public Polynomial sub(Polynomial p) {

    TreeSet<Monomial> lh = new TreeSet<Monomial>(),rh;

    if (p.size() == 0)
      {
        lh.add(new RMonomial(0,0));
        return new Polynomial(lh);
      }
    lh = (TreeSet<Monomial>) poly.clone();
    rh = (TreeSet<Monomial>) p.poly.clone();
    /*if (p.poly.last().deg > poly.last().deg )
      {
        lh = (TreeSet<Monomial>) p.poly.clone();
        rh = (TreeSet<Monomial>) poly.clone();
      }
    else
      {
        rh = (TreeSet<Monomial>) p.poly.clone();
        lh = (TreeSet<Monomial>) poly.clone();
      }*/

    for (Monomial j : rh)
      {
        TSAdd(lh,j.deg,-(j.coef).doubleValue());
      }

    return new Polynomial(lh);
  }

  public Polynomial mul(Polynomial p) {

    if (p.size() == 0)
    {
      TreeSet<Monomial> lh = new TreeSet<Monomial>();
      lh.add(new RMonomial(0,0));
      return new Polynomial(lh);
    }
	  
    TreeSet<Monomial> pres = new TreeSet<Monomial>();

    for(int i = poly.last().deg + p.poly.last().deg ; i >= 0 ; --i)
    {
      Monomial res = new RMonomial(i,0);

      for(Monomial m1 : p.poly)
        {
          for(Monomial m2 : poly)///can easily be replaced with a lower() call in order to reduce from N^3 to N^2*logN
            if (m1.getDeg().intValue() + m2.getDeg().intValue() == i)//check documentation for further info
              {
                res.setCoef(res.getCoef().doubleValue() + m1.getCoef().doubleValue() * m2.getCoef().doubleValue());
              }
        }
      if (res.getCoef().doubleValue() != 0)
        pres.add(res);

    }
    if (pres.size() == 0)
      pres.add(new RMonomial(0,0));

    return new Polynomial(pres);

  }

  public String div(Polynomial p) { // Quotient + Remainder format string

    TreeSet<Monomial> pres = new TreeSet<Monomial>();//will hold  the results
    boolean notzero = true;
    if (p.poly.size() == 0 || poly.size() == 0)//division by 0
      {
        notzero = false;
      }

    Iterator<Monomial> iter1 = p.poly.iterator();

    boolean ok = false;//flag to check again for division by 0
    for (Monomial i : p.poly)
      if (i.getCoef().doubleValue() != 0)
        {
          ok = true;
          break;
        }

    if (ok == false)
      notzero = false;
    //not division by 0, we can continue

    if (notzero == false)
    {
      return "Division by zero!";
    }



    TreeSet<Monomial> lh = new TreeSet<Monomial>();//left hand operand
    TreeSet<Monomial> rh = new TreeSet<Monomial>();//right hand operand
    TreeSet<Monomial> aux = new TreeSet<Monomial>();//auxiliary list which contains current element

    /*if (p.poly.size() <= poly.size()) {//swap polynomials if the second one is greater than the other
      rh = (TreeSet<Monomial>) p.poly.clone();//in order to not change the originals, clones are needed
      lh = (TreeSet<Monomial>) poly.clone();}
    else {
      rh = (TreeSet<Monomial>) poly.clone();
      lh = (TreeSet<Monomial>) p.poly.clone();}*/

    //Uncomment in order to always divide the greater operand by the small one

    rh = (TreeSet<Monomial>) p.poly.clone();
    lh = (TreeSet<Monomial>) poly.clone();

    int ndeg = 1;//new monomial degree
    while(ndeg > 0 && lh.size() != 0)
      {
        Monomial m1 = lh.last() , m2 = rh.last() , res;

        ndeg = m1.getDeg() - m2.getDeg();
        if (ndeg < 0)
          break;

        double ncoef = m1.getCoef().doubleValue() / m2.getCoef().doubleValue();

        res = new RMonomial(ndeg,ncoef);
        pres.add(res);
        aux.clear();
        aux.add(res);

        lh = (new Polynomial(lh).sub(
              new Polynomial(rh).mul(
              new Polynomial(aux)))).poly;
      }

    return "Quotient: " + new Polynomial(pres).toString() + " Remainder: " + new Polynomial(lh).toString();

  }

  public Polynomial integ() {
    TreeSet<Monomial> p,aux = new TreeSet<Monomial>();
    p = (TreeSet<Monomial>) poly.clone();
    for(Monomial i :p)
    {
      if (i.deg + 1 != 0)
      {
        i.coef = i.coef.doubleValue() / (i.deg + 1);
        i.deg ++;
        aux.add(i);
      }
    }
    return new Polynomial(aux);
  }

  public Polynomial diff() {//double differentiation because poly was integrated earlier and cloning does not seem to work?
    TreeSet<Monomial> p,aux = new TreeSet<Monomial>();
    p = (TreeSet<Monomial>) poly.clone();
    for(Monomial i :p)
    {
      i.coef = i.coef.doubleValue() * i.deg;
      i.deg --;
    }
    /*for(Monomial i :p)
    {
      i.coef = i.coef.doubleValue() * i.deg;
      i.deg --;
    }*/
    return new Polynomial(p);
  }

  @Override
  public String toString()
  {
    String result ="";

    if(this != null && this.poly.size()>0)
    {
      Monomial aux = null;
      for (Monomial i : poly)
        if (i.coef.floatValue() != 0f)
          {
              result += i.toString();
              aux = i;
            break;
          }

      boolean ok = false;//if first element was printed

      if (aux != null)
      {
        for(Monomial i : poly)
        {
          if (i == aux)
          {
            ok = true;
          }
          else if (ok == true && i.coef.doubleValue()!=0)
          {
            result+=(i.coef.doubleValue()>0?"+":"") + i.toString();
          }
        }
      }


      if (result.length() == 0)
        {
          result+="0";
        }

    }
    else
      {
        result = "0";
      }
    return result;
  }

}
