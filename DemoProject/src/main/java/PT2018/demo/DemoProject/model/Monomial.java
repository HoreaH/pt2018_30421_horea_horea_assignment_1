package PT2018.demo.DemoProject.model;

public abstract class Monomial implements Cloneable,Comparable{
  
  protected int deg;
  protected Number coef;  
  

  public int compareTo(Monomial other) {///recheck this
      return Integer.compare(this.deg, other.deg);
  }
  
  public Integer getDeg() {
    return deg;
  }

  public void setDeg(int deg) {
    this.deg = deg;
  }

  public Number getCoef() {
    return coef;
  }

  public void setCoef(Number coef) {
    this.coef = coef;
  }

  @Override
  protected Object clone(){
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String toString() {
    // TODO Auto-generated method stub
    return super.toString();//unsure why it works like this
  }
  
  

}
