package PT2018.demo.DemoProject.model;

public class IMonomial extends Monomial implements Cloneable {
  
  public IMonomial(int deg,int coef) {
    this.deg = deg;
    this.coef = coef;
  }
  
  public Integer getCoef() {
    return coef.intValue();
  }

  public void setCoef(Integer coef) {
    this.coef = coef;
  }

  @Override
  protected Object clone(){
    return new IMonomial(this.deg,this.coef.intValue());
  }

  @Override
  public String toString() {
    if (this.deg == 0)
      {
      return this.coef.floatValue() + " "; //for testing purposes
      }
    else if(this.coef.intValue() != 1)
      {
      return this.coef.floatValue() + "x^" + this.deg + " ";
      }
    return "x^" + this.deg;
  }

  public int compareTo(Object o) {
    return Integer.compare(this.deg, ((Monomial)o).deg);
  }
}
