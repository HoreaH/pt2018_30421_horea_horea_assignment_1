package PT2018.demo.DemoProject.model;

import PT2018.demo.DemoProject.controller.Controller;
import PT2018.demo.DemoProject.model.*;
import PT2018.demo.DemoProject.view.View;

public class main {

    public static void main(String[] args) {
        View graphics = new View();

        /*Polynomial p1 = new Polynomial();
        Polynomial p2 = new Polynomial();
        Polynomial[] model = new Polynomial[]{p1,p2};*/

        //391 34 46 142 35 2
        //23 2
        Controller controller = new Controller(graphics/*,model*/);

        graphics.setVisible(true);

    }
}
